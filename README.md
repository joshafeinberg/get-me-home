Get Me Home uses travel options around your city to determine the quickest way for you to get home. Checks availability of taxis as well as any public transit times that you need to know. Just set your home address and get your route home.

With version 2.1 Android Wear support is brought in to allow even quicker way to find your fastest route home.

Created to be an open source project located at https://bitbucket.org/joshafeinberg/get-me-home and released on the MIT license.

Downloadable from [Google Play](https://play.google.com/store/apps/details?id=com.joshafeinberg.trainvstaxi)