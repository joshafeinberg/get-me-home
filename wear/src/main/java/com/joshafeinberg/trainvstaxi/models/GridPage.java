package com.joshafeinberg.trainvstaxi.models;

public class GridPage  {
    private String mTitle;
    private CharSequence mText;
    private int mIcon;
    private int mType;
    private String mProductId;

    public GridPage(String mTitle, String mText, int mIcon, int mType, String productId) {
        this.mTitle = mTitle;
        this.mText = mText;
        this.mIcon = mIcon;
        this.mType = mType;
        this.mProductId = productId;
    }

    public String getTitle() {
        return mTitle;
    }
    public CharSequence getText() {
        return mText;
    }
    public int getIcon() {
        return mIcon;
    }
    public int getType() {
        return mType;
    }
    public String getProductId() {
        return mProductId;
    }
}