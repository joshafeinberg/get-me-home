package com.joshafeinberg.trainvstaxi.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.wearable.activity.ConfirmationActivity;
import android.support.wearable.view.CircledImageView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;
import com.joshafeinberg.constants.Constants;
import com.joshafeinberg.trainvstaxi.GMHApplication;
import com.joshafeinberg.trainvstaxi.R;

import java.util.concurrent.TimeUnit;

public class ActionFragment extends Fragment {

    private int type;
    private int resource;
    private String productId;

    private Boolean intentFired;

    public static ActionFragment create(int type, int resource, String productId) {
        ActionFragment actionFragment = new ActionFragment();
        actionFragment.setType(type);
        actionFragment.setResource(resource);
        actionFragment.setProductId(productId);
        return actionFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.action_layout, null);

        CircledImageView circle = (CircledImageView) view.findViewById(R.id.circle);
        circle.setImageResource(resource);

        TextView actionText = (TextView) view.findViewById(R.id.action_text);
        View layout = view.findViewById(R.id.layout);

        if (type == Constants.UBER) {
            actionText.setText(R.string.get_taxi);
            layout.setBackgroundColor(getResources().getColor(R.color.primary));
        } else {
            actionText.setText(R.string.get_navigation);
            layout.setBackgroundColor(getResources().getColor(R.color.primary_dark));
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            if (type == Constants.UBER) {
                new FireUberIntent().execute(productId);
            } else {
                new FireNavigationIntent().execute();
            }
            }
        });

        return view;
    }

    public void showSuccessActivity() {
        Intent intent = new Intent(getActivity(), ConfirmationActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(ConfirmationActivity.EXTRA_ANIMATION_TYPE, ConfirmationActivity.OPEN_ON_PHONE_ANIMATION);

        getActivity().startActivity(intent);
        getActivity().finish();
    }

    public void showFailureActivity() {
        Intent intent = new Intent(getActivity(), ConfirmationActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(ConfirmationActivity.EXTRA_ANIMATION_TYPE, ConfirmationActivity.FAILURE_ANIMATION);
        // TODO: add real string
        intent.putExtra(ConfirmationActivity.EXTRA_MESSAGE, "Unable to launch");

        getActivity().startActivity(intent);
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setResource(int resource) {
        this.resource = resource;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    private class FireNavigationIntent extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... strings) {
            GoogleApiClient apiClient = GMHApplication.getApiClient();
            if (!apiClient.isConnected()) {
                apiClient.blockingConnect(5, TimeUnit.SECONDS);
            }

            if (!apiClient.isConnected()) {
                return false;
            }

            PutDataMapRequest putDataMapRequest = PutDataMapRequest.create(String.format(Constants.SEND_INTENT, System.currentTimeMillis()));
            putDataMapRequest.getDataMap().putBoolean(Constants.NAV_INTENT, true);
            PutDataRequest request = putDataMapRequest.asPutDataRequest();

            long fired = System.currentTimeMillis();
            Wearable.DataApi.putDataItem(apiClient, request)
                    .setResultCallback(new ResultCallback<DataApi.DataItemResult>() {
                        @Override
                        public void onResult(DataApi.DataItemResult dataItemResult) {
                            intentFired = dataItemResult.getStatus().isSuccess();
                        }
                    }, 5, TimeUnit.SECONDS);

            while (intentFired == null) {
                if (System.currentTimeMillis() - fired > 5000) {
                    intentFired = false;
                }
            }

            return intentFired;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if (success) {
                showSuccessActivity();
            } else {
                showFailureActivity();
            }
        }
    }

    private class FireUberIntent extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... strings) {
            String productId = strings[0];

            GoogleApiClient apiClient = GMHApplication.getApiClient();
            if (!apiClient.isConnected()) {
                apiClient.blockingConnect(5, TimeUnit.SECONDS);
            }

            if (!apiClient.isConnected()) {
                return false;
            }

            PutDataMapRequest putDataMapRequest = PutDataMapRequest.create(String.format(Constants.SEND_INTENT, System.currentTimeMillis()));
            putDataMapRequest.getDataMap().putBoolean(Constants.UBER_INTENT, true);
            putDataMapRequest.getDataMap().putString(Constants.UBER_PRODUCT_ID, productId);
            PutDataRequest request = putDataMapRequest.asPutDataRequest();

            long fired = System.currentTimeMillis();
            Wearable.DataApi.putDataItem(apiClient, request)
                    .setResultCallback(new ResultCallback<DataApi.DataItemResult>() {
                        @Override
                        public void onResult(DataApi.DataItemResult dataItemResult) {
                            intentFired = dataItemResult.getStatus().isSuccess();
                        }
                    }, 5, TimeUnit.SECONDS);

            while (intentFired == null) {
                if (System.currentTimeMillis() - fired > 5000) {
                    intentFired = false;
                }
            }

            return intentFired;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if (success) {
                showSuccessActivity();
            } else {
                showFailureActivity();
            }
        }
    }
}
