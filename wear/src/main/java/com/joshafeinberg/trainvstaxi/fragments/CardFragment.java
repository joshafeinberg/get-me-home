package com.joshafeinberg.trainvstaxi.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.wearable.view.CardScrollView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.joshafeinberg.constants.Constants;
import com.joshafeinberg.trainvstaxi.R;


public class CardFragment extends Fragment {

    private String mTitle;
    private CharSequence mText;
    private int type;

    public static Fragment create(String title, CharSequence text, int type) {
        CardFragment cardFragment = new CardFragment();
        cardFragment.setTitle(title);
        cardFragment.setText(text);
        cardFragment.setType(type);
        return cardFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.card_layout, null);

        CardScrollView cardScrollView = (CardScrollView) view.findViewById(R.id.card_scroll_view);
        cardScrollView.setCardGravity(Gravity.BOTTOM);

        View layout = view.findViewById(R.id.layout);

        if (type == Constants.UBER) {
            layout.setBackgroundColor(getResources().getColor(R.color.primary));
        } else {
            layout.setBackgroundColor(getResources().getColor(R.color.primary_dark));
        }

        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(mTitle);

        TextView description = (TextView) view.findViewById(R.id.description);
        description.setText(mText);

        return view;
    }


    public void setTitle(String title) {
        mTitle = title;
    }

    public void setText(CharSequence text) {
        mText = text;
    }

    public void setType(int type) {
        this.type = type;
    }
}
