package com.joshafeinberg.trainvstaxi.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.wearable.view.GridViewPager;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.DataMap;
import com.joshafeinberg.constants.Constants;
import com.joshafeinberg.trainvstaxi.GMHApplication;
import com.joshafeinberg.trainvstaxi.adapters.GridPagerAdapter;
import com.joshafeinberg.trainvstaxi.models.GridPage;
import com.joshafeinberg.trainvstaxi.R;

import java.util.ArrayList;

public class GridActivity extends Activity {

    private static final String TAG = "GridActivityGMH";

    private GoogleApiClient mGoogleApiClient;

    public static DataMap dataMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.grid_screen);

        mGoogleApiClient = GMHApplication.getApiClient();

        processData();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    private void processData() {

        ArrayList<GridPage> gridPages = new ArrayList<GridPage>();

        Integer max = dataMap.getInt(Constants.SIZE, 0);

        for (int i = 0; i < max; ++i) {
            String title = dataMap.getString(Constants.DISPLAY_NAME + "_" + i);

            int type = dataMap.getInt(Constants.TYPE + "_" + i);
            String nextArrival;
            int drawable;
            String productId = null;

            if (type == Constants.UBER) {
                nextArrival = getString(R.string.next_taxi);
                drawable = R.drawable.ic_uber;
                productId = dataMap.getString(Constants.UBER_PRODUCT_ID + "_" + i);
            } else {
                nextArrival = getString(R.string.next_train);
                drawable = R.drawable.ic_maps_directions_transit;
            }

            nextArrival += " " + dataMap.getString(Constants.ARRIVAL_TIME + "_" + i);

            String travelTime = getString(R.string.travel_time) + " " + dataMap.getString(Constants.TRAVEL_TIME + "_" + i);

            String body = nextArrival + "\n" + travelTime;

            GridPage gridPage = new GridPage(title, body, drawable, type, productId);
            gridPages.add(gridPage);
        }

        GridViewPager pager = (GridViewPager) findViewById(R.id.gridPager);
        pager.setAdapter(new GridPagerAdapter(getFragmentManager(), gridPages));

    }
}
