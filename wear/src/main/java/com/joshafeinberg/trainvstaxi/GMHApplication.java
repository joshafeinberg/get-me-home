package com.joshafeinberg.trainvstaxi;

import android.app.Application;
import android.content.Context;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Wearable;


public class GMHApplication extends Application {

    private static Context sContext;
    private static GoogleApiClient googleApiClient;

    @Override
    public void onCreate() {
        super.onCreate();
        sContext = this;
    }


    public static GoogleApiClient getApiClient() {
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(sContext)
                    .addApi(Wearable.API)
                    .build();
        }
        return googleApiClient;
    }

}
