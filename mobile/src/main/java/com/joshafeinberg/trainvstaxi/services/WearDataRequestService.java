package com.joshafeinberg.trainvstaxi.services;

import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;
import com.joshafeinberg.constants.Constants;
import com.joshafeinberg.trainvstaxi.App;
import com.joshafeinberg.trainvstaxi.adapters.DataAdapter;
import com.joshafeinberg.trainvstaxi.models.ParentData;
import com.joshafeinberg.trainvstaxi.models.SimpleLocation;
import com.joshafeinberg.trainvstaxi.models.cta.CTAData;
import com.joshafeinberg.trainvstaxi.models.uber.UberData;
import com.joshafeinberg.trainvstaxi.utils.DataLookup;
import com.joshafeinberg.trainvstaxi.utils.IntentUtils;
import com.joshafeinberg.trainvstaxi.utils.LocationUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class WearDataRequestService extends WearableListenerService implements DataLookup.DataLookupInterface {

    private static final String TAG = "WearDataRequestServiceGMH";
    private static final Integer MAX_WEAR_ROWS = 20;

    private GoogleApiClient mGoogleApiClient;
    private DataLookup mDataLookup;

    private Boolean intentFired = false;

    @Override
    public void onCreate() {
        super.onCreate();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {

        if (!mGoogleApiClient.isConnected()) {
            ConnectionResult connectionResult = mGoogleApiClient
                    .blockingConnect(30, TimeUnit.SECONDS);
            if (!connectionResult.isSuccess()) {
                Log.e(TAG, "Service failed to connect to GoogleApiClient.");
                return;
            }
        }


        for (DataEvent event : dataEvents) {
            Log.i(TAG, "event => " + event.getDataItem().getUri().getPath());
            if (event.getType() == DataEvent.TYPE_CHANGED) {
                String path = event.getDataItem().getUri().getPath();
                if (Constants.containsPath(Constants.REQUEST_DATA, path)) {
                    handleWearDataRequest();
                }
                if (Constants.containsPath(Constants.SEND_INTENT, path)) {
                    if (intentFired) {
                        return;
                    }
                    intentFired = true;
                    handleIntentRequest(event);
                }
            }
        }
    }

    private void handleWearDataRequest() {
        SimpleLocation destination = LocationUtils.getDestination(App.getContext());
        if (destination == null) {
            sendLocationError();
        } else {
            mDataLookup = new DataLookup(this);
            mDataLookup.getLocationData();
        }
    }

    private void sendLocationError() {
        PutDataMapRequest putDataMapRequest = PutDataMapRequest.create(String.format(Constants.SET_LOCATION_ERROR, System.currentTimeMillis()));
        PutDataRequest request = putDataMapRequest.asPutDataRequest();
        Wearable.DataApi.putDataItem(mGoogleApiClient, request);
    }

    private static int dataReturned = 0;

    @Override
    public void complete() {
        ArrayList<ParentData> data = new ArrayList<>();

        if (dataReturned < 1) {
            ++dataReturned;
            return;
        }

        if (mDataLookup.getDataHashMap() != null) {
            for (Map.Entry<String, UberData> entry : mDataLookup.getDataHashMap().entrySet()) {
                data.add(entry.getValue());
            }
        }

        if (mDataLookup.getCTAData() != null) {
            for (CTAData ctaData : mDataLookup.getCTAData().getCtaDataList()) {
                data.add(ctaData);
            }
        }

        Collections.sort(data, new Comparator<ParentData>() {
            @Override
            public int compare(ParentData parentData, ParentData parentData2) {
                int arrivalTimeCompare = parentData.getArrivalTime().compareTo(parentData2.getArrivalTime());
                if (arrivalTimeCompare != 0) {
                    return arrivalTimeCompare;
                }

                return parentData.getTravelTime().compareTo(parentData2.getTravelTime());
            }
        });

        PutDataMapRequest dataMap = PutDataMapRequest.create(String.format(Constants.SEND_DATA, System.currentTimeMillis()));

        int max = (data.size() > MAX_WEAR_ROWS) ? MAX_WEAR_ROWS : data.size();
        dataMap.getDataMap().putInt(Constants.SIZE, max);

        for (int i = 0; i < max; ++i){
            ParentData parentData = data.get(i);

            dataMap.getDataMap().putString(Constants.DISPLAY_NAME + "_" + i, parentData.getDisplayName());
            dataMap.getDataMap().putString(Constants.ARRIVAL_TIME + "_" + i, DataAdapter.dateFormat(parentData.getArrivalTime()));
            dataMap.getDataMap().putString(Constants.TRAVEL_TIME + "_" + i, DataAdapter.dateFormat(parentData.getTravelTime()));

            if (parentData.isUber()) {
                dataMap.getDataMap().putInt(Constants.TYPE + "_" + i, Constants.UBER);
                dataMap.getDataMap().putString(Constants.UBER_PRODUCT_ID + "_" + i, ((UberData) parentData).getProductId());
            } else if (parentData.isCTA()) {
                dataMap.getDataMap().putInt(Constants.TYPE + "_" + i, Constants.CTA);
            }
        }

        PutDataRequest request = dataMap.asPutDataRequest();
        Wearable.DataApi.putDataItem(mGoogleApiClient, request);
    }

    @Override
    public void showErrorMessage(int message) {
        ++dataReturned;
        sendWearGenericError();
    }

    @Override
    public void locationServicesOff() {
        sendWearGenericError();
    }

    @Override
    public void handleBadLocation() {
        sendWearGenericError();
    }

    @Override
    public void handleNoDestination() {
        sendWearGenericError();
    }

    private void sendWearGenericError() {
        PutDataMapRequest putDataMapRequest = PutDataMapRequest.create(String.format(Constants.SET_LOCATION_ERROR, System.currentTimeMillis()));
        PutDataRequest request = putDataMapRequest.asPutDataRequest();
        Wearable.DataApi.putDataItem(mGoogleApiClient, request);
    }

    private void handleIntentRequest(DataEvent event) {
        DataMapItem dataMapItem = DataMapItem.fromDataItem(event.getDataItem());
        if (dataMapItem.getDataMap().containsKey(Constants.NAV_INTENT)) {
            // start addresses should all be the same so we can safely send the first one in the list
            IntentUtils.sendNavigationIntent(this, mDataLookup.getCTAData().getCtaDataList().get(0).getStartAddress());
        } else if (dataMapItem.getDataMap().containsKey(Constants.UBER_INTENT)) {
            String productId = dataMapItem.getDataMap().getString(Constants.UBER_PRODUCT_ID);
            IntentUtils.sendUberIntent(this, productId);
        }
    }
}

