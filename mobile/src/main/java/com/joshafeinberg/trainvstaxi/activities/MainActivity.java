package com.joshafeinberg.trainvstaxi.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.joshafeinberg.trainvstaxi.R;
import com.joshafeinberg.trainvstaxi.fragments.MainActivityFragment;


public class MainActivity extends FragmentActivity {

    private static final String TAG = "mobile";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MainActivityFragment fragment = (MainActivityFragment) getSupportFragmentManager().findFragmentByTag("mainFragment");

        if (fragment == null) {
            fragment = new MainActivityFragment();
        }

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, fragment, "mainFragment")
                    .commit();
        }
    }

}