package com.joshafeinberg.trainvstaxi.fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.joshafeinberg.trainvstaxi.R;
import com.joshafeinberg.trainvstaxi.adapters.DataAdapter;
import com.joshafeinberg.trainvstaxi.models.SimpleLocation;
import com.joshafeinberg.trainvstaxi.models.cta.CTAList;
import com.joshafeinberg.trainvstaxi.models.uber.UberData;
import com.joshafeinberg.trainvstaxi.utils.DataLookup;
import com.joshafeinberg.trainvstaxi.utils.LocationUtils;

import java.util.HashMap;
import java.util.Map;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

public class MainActivityFragment extends Fragment implements HomeAddressDialog.NewAddressSetListener,
                                                              Toolbar.OnMenuItemClickListener,
                                                              SwipeRefreshLayout.OnRefreshListener,
                                                              DataLookup.DataLookupInterface {

    private MenuItem mRefreshMenuItem;

    private Toolbar mToolbar;
    private RecyclerView mRecyclerView;
    private ProgressDialog mProgressDialog;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    private DataLookup mDataLookup;

    private static final String UBER_DATA = "uberData";
    private static final String CTA_DATA = "ctaData";

    public MainActivityFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        mDataLookup = new DataLookup(this);

        if (savedInstanceState == null) {
            showProgressDialog();
        } else {
            Gson gson = new Gson();
            @SuppressWarnings("unchecked")
            HashMap<String, String> tempData = (HashMap<String, String>) savedInstanceState.getSerializable(UBER_DATA);
            if (tempData != null && tempData.size() > 0) {
                mDataLookup.setDataHashMap(new HashMap<String, UberData>());
                for (Map.Entry<String, String> entry : tempData.entrySet()) {
                    mDataLookup.getDataHashMap().put(entry.getKey(), gson.fromJson(entry.getValue(), UberData.class));
                }
            }
            mDataLookup.setCTAData(gson.fromJson(savedInstanceState.getString(CTA_DATA), CTAList.class));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);

        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        mToolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        mToolbar.setOnMenuItemClickListener(this);
        mToolbar.inflateMenu(R.menu.main);
        mRefreshMenuItem = mToolbar.getMenu().findItem(R.id.action_refresh);
        mToolbar.setTitle(R.string.app_name);
        mToolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
        mToolbar.setVisibility(View.GONE);

        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setProgressViewOffset(true, -1000, -1000);

        return rootView;
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        int id = menuItem.getItemId();
        switch (id) {
            case R.id.action_refresh:
                refreshData();
                break;
            case R.id.action_setHome:
                setHome();
                break;
            case R.id.action_about:
                showAbout();
                return true;
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void newAddressSet() {
        if (mDataLookup != null && mDataLookup.getCTAData() != null) {
            mDataLookup.getCTAData().setLastUpdateTime(0L);
            mDataLookup.updateDestination();
        }
        showProgressDialog();
        refreshData();
    }

    @Override
    public void onRefresh() {
        refreshData();
    }

    /**
     * calls refresh on the screen
     * uses {@link com.joshafeinberg.trainvstaxi.utils.DataLookup#REFRESH_TIME} value to limit google calls
     */
    public void refreshData() {
        // creates image rotation
        LayoutInflater inflater = getLayoutInflater(null);
        ImageView refreshIcon = (ImageView) inflater.inflate(R.layout.rotate_refresh, (ViewGroup) getView(), false);
        Animation rotation = AnimationUtils.loadAnimation(getActivity(), R.anim.rotate);
        if (rotation != null && refreshIcon != null && mRefreshMenuItem != null) {
            rotation.setRepeatCount(Animation.INFINITE);
            refreshIcon.startAnimation(rotation);
            mRefreshMenuItem.setActionView(refreshIcon);
        }

        mDataLookup.getLocationData();
    }

    public void showProgressDialog() {
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage(getString(R.string.loading_generic));
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public void hideProgress() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }


    /**
     * set home address to use google api to determine travel
     */
    public void setHome() {
        getChildFragmentManager().beginTransaction().add(new HomeAddressDialog(), "SETHOME").commit();
    }

    private void showAbout() {
        @SuppressLint("InflateParams")
        View aboutView = LayoutInflater.from(getActivity()).inflate(R.layout.about_app, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.aboutapp);
        builder.setView(aboutView);
        builder.setPositiveButton(android.R.string.ok, null);
        builder.show();
    }

    @Override
    public void onResume() {
        super.onResume();

        //check on boot if home is set, else need to set it
        mDataLookup.updateDestination();
        SimpleLocation destination = LocationUtils.getDestination(getActivity());
        if (destination == null) {
            hideProgress();
            setHome();
        } else if (isUberMapComplete() && mDataLookup.getCTAData().isComplete()) {
            updateAdapter();
        } else {
            refreshData();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (isUberMapComplete() && mDataLookup.getCTAData().isComplete()) {
            Gson gson = new Gson();
            // UberData is not serializable so convert it to a json string first
            HashMap<String, String> tempDataHashMap = new HashMap<String, String>();
            for (Map.Entry<String, UberData> entry : mDataLookup.getDataHashMap().entrySet()) {
                tempDataHashMap.put(entry.getKey(), gson.toJson(entry.getValue()));
            }
            outState.putSerializable(UBER_DATA, tempDataHashMap);
            outState.putString(CTA_DATA, gson.toJson(mDataLookup.getCTAData()));
        }
    }

    @Override
    public void locationServicesOff() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.error_location_off);
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                getActivity().finish();
            }
        });
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent viewIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(viewIntent);
            }
        });
        builder.show();
    }

    @Override
    public void handleBadLocation() {
        stopRotation();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.error_bad_location);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                setHome();
            }
        });
    }

    @Override
    public void complete() {
        updateAdapter();
    }

    public void updateAdapter() {
        RecyclerView.Adapter adapter = new DataAdapter(getActivity(), mDataLookup.getDataHashMap(), mDataLookup.getCTAData());
        mRecyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        stopRotation();
        mRecyclerView.setVisibility(View.VISIBLE);
        mToolbar.setVisibility(View.VISIBLE);
        hideProgress();
    }

    public boolean isUberMapComplete() {
        if (mDataLookup.getDataHashMap() == null) {
            return false;
        }
        for (Map.Entry<String, UberData> entry : mDataLookup.getDataHashMap().entrySet()) {
            if (!entry.getValue().isComplete()) {
                return false;
            }
        }

        return true;
    }

    @Override
    public void handleNoDestination() {
        Toast.makeText(getActivity(), R.string.setaddress, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showErrorMessage(int message) {
        showErrorMessage(getString(message));
    }

    public void showErrorMessage(String message) {
        Crouton.makeText(getActivity(), message + " " + getString(R.string.retry), Style.ALERT).show();
        hideProgress();
        stopRotation();
    }

    /**
     * stop the rotation once all responses are received
     */
    public void stopRotation() {
        if (mRefreshMenuItem != null && mRefreshMenuItem.getActionView() != null) {
            mRefreshMenuItem.getActionView().clearAnimation();
            mRefreshMenuItem.setActionView(null);
        }
        mSwipeRefreshLayout.setRefreshing(false);
    }
}