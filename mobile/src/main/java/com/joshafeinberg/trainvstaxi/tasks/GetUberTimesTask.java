package com.joshafeinberg.trainvstaxi.tasks;

import android.os.AsyncTask;

import com.joshafeinberg.trainvstaxi.App;
import com.joshafeinberg.trainvstaxi.models.uber.UberTimes;
import com.joshafeinberg.trainvstaxi.network.RestApiAdapter;
import com.joshafeinberg.trainvstaxi.network.UberRestClient;

import java.util.LinkedHashMap;

public class GetUberTimesTask extends AsyncTask<Double, Void, UberTimes> {

    public static interface OnCompleteListener {
        public void onTimesFetched(UberTimes response);
    }

    private OnCompleteListener mOnCompleteListener;

    public void setOnCompleteListener(OnCompleteListener onCompleteListener) {
        mOnCompleteListener = onCompleteListener;
    }

    @Override
    protected UberTimes doInBackground(Double... params) {
        Double longitude = params[0];
        Double latitude = params[1];

        UberRestClient uberRestClient = RestApiAdapter.getInstance().getUberRestClient();

        LinkedHashMap<String, String> options = new LinkedHashMap<String, String>();
        options.put("server_token", App.UBER_API_KEY);
        options.put("start_longitude", Double.toString(longitude));
        options.put("start_latitude", Double.toString(latitude));

        return uberRestClient.getUberTimes(options);
    }

    @Override
    protected void onPostExecute(UberTimes response) {
        mOnCompleteListener.onTimesFetched(response);
    }
}
