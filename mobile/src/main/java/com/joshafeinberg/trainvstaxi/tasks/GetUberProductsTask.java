package com.joshafeinberg.trainvstaxi.tasks;

import android.os.AsyncTask;

import com.joshafeinberg.trainvstaxi.App;
import com.joshafeinberg.trainvstaxi.models.uber.UberProducts;
import com.joshafeinberg.trainvstaxi.network.RestApiAdapter;
import com.joshafeinberg.trainvstaxi.network.UberRestClient;

import java.util.LinkedHashMap;

public class GetUberProductsTask extends AsyncTask<Double, Void, UberProducts> {

    public static interface OnCompleteListener {
        public void onProductsFetched(UberProducts response);
    }

    private OnCompleteListener mOnCompleteListener;

    public void setOnCompleteListener(OnCompleteListener onCompleteListener) {
        mOnCompleteListener = onCompleteListener;
    }

    @Override
    protected UberProducts doInBackground(Double... params) {
        Double longitude = params[0];
        Double latitude = params[1];

        UberRestClient uberRestClient = RestApiAdapter.getInstance().getUberRestClient();

        LinkedHashMap<String, String> options = new LinkedHashMap<String, String>();
        options.put("server_token", App.UBER_API_KEY);
        options.put("longitude", Double.toString(longitude));
        options.put("latitude", Double.toString(latitude));

        return uberRestClient.getUberProducts(options);
    }

    @Override
    protected void onPostExecute(UberProducts response) {
        mOnCompleteListener.onProductsFetched(response);
    }
}
