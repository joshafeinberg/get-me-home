package com.joshafeinberg.trainvstaxi.models.cta;

import android.graphics.drawable.Drawable;

import com.joshafeinberg.trainvstaxi.App;
import com.joshafeinberg.trainvstaxi.R;
import com.joshafeinberg.trainvstaxi.models.ParentData;

public class CTAData extends ParentData {
    private Integer travelTime;
    private Integer arrivalTime;
    private CharSequence route;
    private String startAddress;

    public Integer getTravelTime() {
        return travelTime;
    }

    public void setTravelTime(Integer travelTime) {
        this.travelTime = travelTime;
    }

    public Integer getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Integer arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    @Override
    public DATA_TYPE getType() {
        return DATA_TYPE.CTA;
    }

    @Override
    public String getDisplayName() {
        return App.getContext().getString(R.string.public_transit);
    }

    public Drawable getImage() {
        return App.getContext().getResources().getDrawable(R.drawable.ic_maps_directions_transit);
    }

    public CharSequence getRoute() {
        return route;
    }

    public void setRoute(CharSequence route) {
        this.route = route;
    }

    public String getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(String startAddress) {
        this.startAddress = startAddress;
    }
}
