package com.joshafeinberg.trainvstaxi.models.uber;

import com.google.gson.annotations.Expose;

import java.util.List;

public class UberPrices {

    @Expose
    private List<UberPrice> prices;

    public List<UberPrice> getPrices() {
        return prices;
    }
}
