package com.joshafeinberg.trainvstaxi.models.google;


import com.google.gson.annotations.Expose;

public class KeyValue {

    @Expose
    private Integer value;
    @Expose
    private String text;

    public Integer getValue() {
        return value;
    }
}
