package com.joshafeinberg.trainvstaxi.models.uber;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UberProduct {

    @SerializedName("product_id")
    @Expose
    private String productId;
    @Expose
    private String description;
    @SerializedName("display_name")
    @Expose
    private String displayName;
    @Expose
    private Integer capacity;
    @Expose
    private String image;

    public String getProductId() {
        return productId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public String getImage() {
        return image;
    }
}
