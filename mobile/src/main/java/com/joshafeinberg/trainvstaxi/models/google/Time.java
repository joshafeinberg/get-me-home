package com.joshafeinberg.trainvstaxi.models.google;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Time {

    @Expose
    private String text;
    @SerializedName("time_zone")
    @Expose
    private String timeZone;
    @Expose
    private Long value;

    public Long getValue() {
        return value;
    }
}
