package com.joshafeinberg.trainvstaxi.models.uber;

import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.style.StyleSpan;

import com.joshafeinberg.trainvstaxi.models.ParentData;

public class UberData extends ParentData {

    // products
    private String productId;
    private String displayName;
    private Integer capacity;
    private String image;

    // prices
    private String priceEstimate;
    private Integer duration;

    // times
    private Integer timeEstimate;

    private Boolean isComplete = false;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public String getImageUrl() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public CharSequence getPriceEstimate() {
        if (priceEstimate == null) {
            return "";
        }
        SpannableString price = new SpannableString(priceEstimate);
        price.setSpan(new StyleSpan(Typeface.BOLD), 0, price.length(), 0);
        return price;
    }

    public void setPriceEstimate(String priceEstimate) {
        this.priceEstimate = priceEstimate;
    }

    public Integer getTravelTime() {
        if (duration == null) {
            duration = 0;
        }
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getArrivalTime() {
        if (timeEstimate == null) {
            timeEstimate = 0;
        }
        return timeEstimate;
    }

    public void setTimeEstimate(Integer timeEstimate) {
        this.timeEstimate = timeEstimate;
    }

    @Override
    public DATA_TYPE getType() {
        return DATA_TYPE.UBER;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete() {
        isComplete = true;
    }
}
