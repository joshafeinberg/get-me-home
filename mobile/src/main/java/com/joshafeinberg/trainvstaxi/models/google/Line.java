package com.joshafeinberg.trainvstaxi.models.google;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Line {

    @Expose
    private String color;
    @Expose
    private String name;
    @SerializedName("short_name")
    @Expose
    private String shortName;

    public String getColor() {
        return color;
    }

    public String getName() {
        return name;
    }

    public String getShortName() {
        return shortName;
    }
}
