package com.joshafeinberg.trainvstaxi.utils;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;

import com.joshafeinberg.trainvstaxi.App;
import com.joshafeinberg.trainvstaxi.models.SimpleLocation;

/**
 * Created by solarmoo900 on 12/6/14.
 */
public class IntentUtils {

    private static final String GMAPS_URL = "http://maps.google.com/maps?saddr=%s&daddr=%s&dirflg=r";
    private static final String UBER_URL = "https://m.uber.com/sign-up?client_id=%s";
    private static final String UBER_APP_URL = "uber://?client_id=%s&action=%s&pickup=%s&dropoff[latitude]=%f&dropoff[longitude]=%f&dropoff[formatted_address]=%s&product_id=%s;";

    public static void sendNavigationIntent(Context context, String startAddress) {
        SimpleLocation simpleLocation = LocationUtils.getDestination(context);
        String url = String.format(GMAPS_URL, startAddress, simpleLocation.getAddress());
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(url));
        if (context instanceof Service) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        context.startActivity(intent);
    }

    public static void sendUberIntent(Context context, String productId) {
        PackageManager pm = context.getPackageManager();
        Intent intent;
        try {
            pm.getPackageInfo("com.ubercab", PackageManager.GET_ACTIVITIES);

            String clientId = App.UBER_CLIENT_ID;
            String action = "setPickup";
            String pickup = "my_location";
            SimpleLocation simpleLocation = LocationUtils.getDestination(context);
            Double dropoffLatitude = simpleLocation.getLatitude();
            Double dropoffLongitude = simpleLocation.getLongitude();
            String dropoffAddress = simpleLocation.getFormattedAddress();
            String url = String.format(UBER_APP_URL, clientId, action, pickup, dropoffLatitude, dropoffLongitude, dropoffAddress, productId);
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        }
        catch (PackageManager.NameNotFoundException e) {
            String url = String.format(UBER_URL, App.UBER_CLIENT_ID);
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        }

        if (context instanceof Service) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }

        context.startActivity(intent);
    }
}
