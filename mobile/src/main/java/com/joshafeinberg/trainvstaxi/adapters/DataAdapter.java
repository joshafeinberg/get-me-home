package com.joshafeinberg.trainvstaxi.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.joshafeinberg.trainvstaxi.App;
import com.joshafeinberg.trainvstaxi.R;
import com.joshafeinberg.trainvstaxi.models.ParentData;
import com.joshafeinberg.trainvstaxi.models.SimpleLocation;
import com.joshafeinberg.trainvstaxi.models.cta.CTAData;
import com.joshafeinberg.trainvstaxi.models.cta.CTAList;
import com.joshafeinberg.trainvstaxi.models.uber.UberData;
import com.joshafeinberg.trainvstaxi.utils.IntentUtils;
import com.joshafeinberg.trainvstaxi.utils.LocationUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {

    private Context mContext;
    private List<ParentData> mData;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public View mLayout;
        public ImageView mImage;
        public TextView mRateType;
        public TextView mCapacity;
        public TextView mArrivalTime;
        public TextView mTravelTime;
        public TextView mCost;
        public TextView mTravelRoute;
        public TextView mArrivalHeader;
        public ViewHolder(View v) {
            super(v);
            mLayout = v;
            mImage = (ImageView) v.findViewById(R.id.transport_image);
            mCapacity = (TextView) v.findViewById(R.id.transport_capacity);
            mRateType = (TextView) v.findViewById(R.id.transport_type);
            mArrivalTime = (TextView) v.findViewById(R.id.transport_arrival_time);
            mTravelTime = (TextView) v.findViewById(R.id.transport_travel_time);
            mCost = (TextView) v.findViewById(R.id.transport_travel_cost);
            mTravelRoute = (TextView) v.findViewById(R.id.transport_travel_route);
            mArrivalHeader = (TextView) v.findViewById(R.id.transport_arrival_header);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public DataAdapter(Context context, HashMap<String, UberData> dataHashMap, CTAList ctaList) {
        mContext = context;

        mData = new ArrayList<ParentData>();

        if (dataHashMap != null) {
            for (Map.Entry<String, UberData> entry : dataHashMap.entrySet()) {
                mData.add(entry.getValue());
            }
        }

        if (ctaList != null) {
            for (CTAData ctaData : ctaList.getCtaDataList()) {
                mData.add(ctaData);
            }
        }

        Collections.sort(mData, new Comparator<ParentData>() {
            @Override
            public int compare(ParentData parentData, ParentData parentData2) {
                int arrivalTimeCompare = parentData.getArrivalTime().compareTo(parentData2.getArrivalTime());
                if (arrivalTimeCompare != 0) {
                    return arrivalTimeCompare;
                }

                return parentData.getTravelTime().compareTo(parentData2.getTravelTime());
            }
        });
    }

    // Create new views (invoked by the layout manager)
    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);

        return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        ParentData data = mData.get(position);

        holder.mRateType.setText(data.getDisplayName());
        holder.mArrivalTime.setText(dateFormat(data.getArrivalTime()));
        if (data.getTravelTime().equals(-1)) {
            holder.mTravelTime.setText(mContext.getString(R.string.travel_time) + mContext.getString(R.string.time_unavailable));
        } else {
            holder.mTravelTime.setText(mContext.getString(R.string.travel_time) + dateFormat(data.getTravelTime()));
        }

        if (data.isUber()) {
            UberData uberData = (UberData) data;

            Picasso.with(mContext).load(uberData.getImageUrl()).into(holder.mImage);

            holder.mCapacity.setVisibility(View.VISIBLE);
            holder.mCapacity.setText("(" + uberData.getCapacity() + ")");

            holder.mCost.setVisibility(View.VISIBLE);
            holder.mCost.setText(TextUtils.concat(mContext.getString(R.string.estimated_cost), uberData.getPriceEstimate()), TextView.BufferType.SPANNABLE);

            holder.mArrivalHeader.setText(mContext.getString(R.string.next_taxi));
        }

        if (data.isCTA()) {
            CTAData ctaData = (CTAData) data;

            holder.mImage.setImageDrawable(ctaData.getImage());

            holder.mTravelRoute.setVisibility(View.VISIBLE);
            holder.mTravelRoute.setText(ctaData.getRoute(), TextView.BufferType.SPANNABLE);

            holder.mArrivalHeader.setText(mContext.getString(R.string.next_train));
        }

        setOnClickListener(holder, data);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mData.size(); // CTA data
    }

    /**
     * converts time to more readable format
     * @param seconds time
     * @return string containing formatted date
     */
    public static String dateFormat(int seconds) {

        // convert to even minutes for pretty display
        int conversionSeconds = 0;
        while (conversionSeconds < seconds) {
            conversionSeconds += 60;
        }

        int minutes = conversionSeconds / 60;

        return minutes + " " + App.getContext().getResources().getQuantityString(R.plurals.minutes, minutes);
    }

    public void setOnClickListener(ViewHolder viewHolder, final ParentData data) {
        View layout = viewHolder.mLayout;

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (data.isUber()) {
                    UberData uberData = (UberData) data;
                    generateUberClickListener(uberData.getProductId());
                } else if (data.isCTA()) {
                    CTAData ctaData = (CTAData) data;
                    showDialogForDirections(ctaData.getStartAddress());
                }
            }
        });
    }

    private void generateUberClickListener(String productId) {
        IntentUtils.sendUberIntent(mContext, productId);
    }

    public void showDialogForDirections(final String startAddress) {
        final SimpleLocation simpleLocation = LocationUtils.getDestination(mContext);

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(R.string.gotonav);
        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (simpleLocation == null) {
                    Toast.makeText(mContext, R.string.setaddress, Toast.LENGTH_LONG).show();
                    return;
                }
                // start google navigation
                IntentUtils.sendNavigationIntent(mContext, startAddress);
            }
        });
        builder.setNegativeButton(android.R.string.no, null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }


}