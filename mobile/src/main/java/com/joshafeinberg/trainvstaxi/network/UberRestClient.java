package com.joshafeinberg.trainvstaxi.network;


import com.joshafeinberg.trainvstaxi.models.uber.UberPrices;
import com.joshafeinberg.trainvstaxi.models.uber.UberProducts;
import com.joshafeinberg.trainvstaxi.models.uber.UberTimes;

import java.util.Map;

import retrofit.http.GET;
import retrofit.http.QueryMap;

public interface UberRestClient {

    @GET("/v1/products")
    UberProducts getUberProducts(@QueryMap Map<String, String> options);

    @GET("/v1/estimates/price")
    UberPrices getUberPrices(@QueryMap Map<String, String> options);

    @GET("/v1/estimates/time")
    UberTimes getUberTimes(@QueryMap Map<String, String> options);
}
