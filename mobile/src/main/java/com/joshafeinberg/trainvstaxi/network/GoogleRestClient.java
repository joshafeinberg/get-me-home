package com.joshafeinberg.trainvstaxi.network;


import com.joshafeinberg.trainvstaxi.models.google.Predictions;
import com.joshafeinberg.trainvstaxi.models.google.TravelTime;

import java.util.Map;

import retrofit.http.GET;
import retrofit.http.QueryMap;

public interface GoogleRestClient {

    @GET("/maps/api/directions/json")
    TravelTime getGoogleMapDirections(@QueryMap Map<String, String> options);

    @GET("/maps/api/place/autocomplete/json")
    Predictions getAutosuggestion(@QueryMap Map<String, String> options);
}
