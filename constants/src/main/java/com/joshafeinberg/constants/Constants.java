package com.joshafeinberg.constants;

/**
 * Created by solarmoo900 on 12/5/14.
 */
public class Constants {

    // Wear Requests
    public static final String REQUEST_DATA = "/request_data_for_wear/%d";

    // Mobile Returns
    public static final String SET_LOCATION_ERROR = "/set_location_error/%d";
    public static final String GENERIC_ERROR = "/set_location_error/%d";
    public static final String SEND_DATA = "/send_data_to_wear/%d";
    public static final String SEND_INTENT = "/send_intent_request/%d";

    // Data Fields
    public static final String SIZE = "size";

    public static final String TYPE = "type";
    public static final Integer UBER = 0;
    public static final Integer CTA = 1;

    public static final String DISPLAY_NAME = "display_name";
    public static final String ARRIVAL_TIME = "arrival_time";
    public static final String TRAVEL_TIME = "travel_time";

    public static final String UBER_PRODUCT_ID = "product_id";

    // Intent Fields
    public static final String NAV_INTENT = "navigation_intent";
    public static final String UBER_INTENT = "uber_intent";

    // compare
    public static Boolean containsPath(String path1, String path2) {
        return path2.startsWith(path1.substring(0, path1.length() - 3));
    }
}
